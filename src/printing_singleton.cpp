#include "printing_singleton.hpp"

#include <cups/cups.h>
#include <podofo/podofo.h>
#include <iostream>
#include <filesystem>

#include <godot_cpp/classes/image.hpp>
#include <godot_cpp/classes/polygon2d.hpp>
#include <godot_cpp/classes/sprite2d.hpp>
#include <godot_cpp/classes/path2d.hpp>
#include <godot_cpp/classes/line2d.hpp>
#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/variant/utility_functions.hpp>


using namespace std;
using namespace godot;
using namespace PoDoFo;


PrintingSingleton *PrintingSingleton::singleton = nullptr;

//forward dec
int PrintWithCups(const char *filename,
		  cups_dest_t *dest,
		  cups_dinfo_t *dest_info,
		  const char *title,
		  int num_copies
		 );


void PrintingSingleton::_bind_methods()
{
	ClassDB::bind_method(D_METHOD("hello_singleton"), &PrintingSingleton::hello_singleton);
	ClassDB::bind_method(D_METHOD("GetCupsDestinations"), &PrintingSingleton::GetCupsDestinations);
	ClassDB::bind_method(D_METHOD("Print", "canvas", "destination", "is_to_file"), &PrintingSingleton::Print);
}

PrintingSingleton *PrintingSingleton::get_singleton()
{
	return singleton;
}

PrintingSingleton::PrintingSingleton()
{
	ERR_FAIL_COND(singleton != nullptr);
	singleton = this;
}

PrintingSingleton::~PrintingSingleton()
{
	ERR_FAIL_COND(singleton != this);
	singleton = nullptr;
}

void PrintingSingleton::hello_singleton()
{
	UtilityFunctions::print("Hello GDExtension Singleton!");
}


//--------------------------------- Print Destinations ------------------------------


godot::TypedArray<String> PrintingSingleton::GetCupsDestinations()
{
	godot::TypedArray<String> list;

	cups_dest_t *dests = nullptr;
	int numdests = cupsGetDests2(CUPS_HTTP_DEFAULT, &dests);

	//-----just keeping this block around for reference:
	cerr << "-----------Printer Info--------------"<<endl;
	if (numdests) {
		for (int c=0; c<numdests; c++) {
			cerr << "dest "<<c<<": "<<dests[c].name;
			list.push_back(dests[c].name);
			if (dests[c].instance) cerr<<"/"<<dests[c].instance;
			cerr<<endl;
		}
	} else {
		cerr << "No dests!"<<endl;
	}
	
	int current_printer = 0;

	if (current_printer >= 0 && current_printer < numdests) {
		cerr << "options:"<<endl;
		for (int c=0; c<dests[current_printer].num_options; c++) {
			cerr << "  "<<dests[current_printer].options[c].name<<"="<<dests[current_printer].options[c].value<<endl;
		}
		cerr << "-----------specific options:--------------"<<endl;

		cups_dinfo_t *dest_info = cupsCopyDestInfo(CUPS_HTTP_DEFAULT, &(dests[0])); // **** does this need to be freed?

		if (cupsCheckDestSupported(CUPS_HTTP_DEFAULT, &(dests[0]), dest_info,
		                           CUPS_FINISHINGS, NULL))
		{
			ipp_attribute_t *finishings = cupsFindDestSupported(CUPS_HTTP_DEFAULT, &dests[0], dest_info, CUPS_FINISHINGS);
			int count = ippGetCount(finishings);

			cerr << "  finishings supported: "<<count<<endl;
			for (int i = 0; i < count; i ++)
				cerr << "    "<< ippGetInteger(finishings, i)<<endl;
		}
		else cerr<< "  finishings not supported."<<endl;
	}

	cupsFreeDests(numdests, dests);
	return list;
}



//--------------------------------- Print ------------------------------

int PrintingSingleton::Print(Node2D *canvas, String destination, bool is_to_file, float offset_y)
{
	cout << "PrintingSingleton::Print: " << destination.utf8() <<", is_to_file: " << is_to_file <<endl;

	//auto type = canvas.get_type();
	//String type_name = Variant::get_type_name(type);
	//cout << "Canvas type: "<<type_name.utf8()<<endl;

	//Node canvas_node = Object::cast_to<Node>(canvas);

	int num_kids = canvas->get_child_count();
	//cout << "num canvas kids: "<<num_kids<<endl;
	UtilityFunctions::print("num canvas kids: " + String::num_int64(num_kids));

	Node2D *paper_child = nullptr;
	for (int c=0; c<num_kids; c++) {
		Node *child = canvas->get_child(c);
		if (child->get_name() == String("Paper")) {
			paper_child = Object::cast_to<Node2D>(child);
			break;
		}
	}

	float page_width  = 8.5;
	float page_height = 11.0;

	if (paper_child) {
		Variant w = paper_child->get("width");
		float v = w;
		if (v > 0) page_width = v;
		Variant h = paper_child->get("height");
		v = h;
		if (v > 0) page_height = v;
		cout << "found paper size: "<<page_width <<" x "<<page_height<<endl;
	}


    // The document is written directly to filename while being created.
    PdfMemDocument document;

    // PdfPainter is the class which is able to draw text and graphics
    // directly on a PdfPage object.
    PdfPainter painter;

    // A PdfFont object is required to draw text on a PdfPage using a PdfPainter.
    // PoDoFo will find the font using fontconfig on your system and embedd truetype
    // fonts automatically in the PDF file.
    PdfFont* font;

    try
    {
        // The PdfDocument object can be used to create new PdfPage objects.
        // The PdfPage object is owned by the PdfDocument will also be deleted automatically
        // by the PdfDocument object.
        // 
        // You have to pass only one argument, i.e. the page size of the page to create.
        // There are predefined enums for some common page sizes.
        PoDoFo::Rect rect(0,0, 72*page_width, 72*page_height);
        auto& page = document.GetPages().CreatePage(rect);
        //auto& page = document.GetPages().CreatePage(PdfPage::CreateStandardPageSize(PdfPageSize::A4));

        // Set the page as drawing target for the PdfPainter.
        // Before the painter can draw, a page has to be set first.
        painter.SetCanvas(page);

        // // Create a PdfFont object using the font "Arial".
        // // The font is found on the system using fontconfig and embedded into the
        // // PDF file. If Arial is not available, a default font will be used.
        // // 
        // // The created PdfFont will be deleted by the PdfDocument.
        // font = document.GetFonts().SearchFont("Arial");

        // // If the PdfFont object cannot be allocated return an error.
        // //if (font == nullptr)
        // //    throw runtime_error("Invalid handle");  **** aaaa! no exceptions in Godot!!
		
		// if (font != nullptr) {
	    //     auto& metrics = font->GetMetrics();
	    //     cout << "The font name is "<< metrics.GetFontName() << endl;
	    //     cout << "The family font name is " << metrics.GetFontFamilyName() << endl;
	    //     cout << "The font file path is " << metrics.GetFilePath() << endl;
	    //     cout << "The font face index is " << metrics.GetFaceIndex() << endl;

	    //     // Set the font as default font for drawing.
	    //     // A font has to be set before you can draw text on
	    //     // a PdfPainter.
	    //     painter.TextState.SetFont(*font, 18);

	    //     // You could set a different color than black to draw
	    //     // the text.
	    //     // 
	    //     // painter.SetColor(1.0, 0.0, 0.0);

	    //     // Actually draw the line "Hello World!" on to the PdfPage at
	    //     // the position 2cm,2cm from the top left corner.
	    //     // Please remember that PDF files have their origin at the
	    //     // bottom left corner. Therefore we substract the y coordinate
	    //     // from the page height.
	    //     // 
	    //     // The position specifies the start of the baseline of the text.
	    //     // 
	    //     // All coordinates in PoDoFo are in PDF units.
	    //     painter.DrawText("ABCDEFGHIKLMNOPQRSTVXYZ", 56.69, page.GetRect().Height - 56.69);

	    //     //try
	    //     //{
	    //     //    // Add also some non-ASCII characters (Cyrillic alphabet)
	    //     //    painter.DrawText("АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЫЭЮЯ", 56.69, page.GetRect().Height - 80);
	    //     //}
		// 	//catch (PdfError& err)
	    //     //{
	    //     //    if (err.GetCode() == PdfErrorCode::InvalidFontData)
	    //     //        cout << "WARNING: The matched font \"" << metrics.GetFontName() << "\" doesn't support cyrillic" << endl;
	    //     //}
        // }

        PdfColor stroke_color(1.0, 0., 0.);
        PdfColor fill_color(0.,0.,1.);
        painter.GraphicsState.SetStrokeColor(stroke_color);
        painter.GraphicsState.SetFillColor(fill_color);
        //painter.DrawCircle(72*3,72*3, 72*1.5);
        //painter.DrawCircle(72*3,72*3, 72*1.0, PdfPathDrawMode::Fill);


		Transform2D paper_tr(72, 0, 0, -72, 0, 72*page_height + offset_y);

		for (int c=0; c<num_kids; c++) {
			Node2D *child = Object::cast_to<Node2D>(canvas->get_child(c));
			if (!child || child == paper_child) continue;

			String name = child->get_name();
			cout <<"child: "<<name.utf8()<<endl;

			//Transform2D paper_tr(72, 0, 0, -72, 0, 72*page_height);
			//Transform2D paper_tr(72, 0, 0, -72, 0, -72*page_height);
			//Transform2D paper_tr(72, 0, 0, -72, 0, 0);
			//Matrix matrix; //= Matrix::FromCoefficients(72, 0, 0, -72, 0, page_height); //flip vertically and scale to pdf units = 72/inch
			//matrix = painter.GraphicsState.GetCurrentMatrix();
			//matrix = Matrix();
			
			painter.Save();
			Transform2D tr = paper_tr * paper_child->get_global_transform().affine_inverse() * child->get_global_transform();
			Matrix matrix = Matrix::FromCoefficients(tr.columns[0].x, tr.columns[0].y, tr.columns[1].x, tr.columns[1].y, tr.columns[2].x, tr.columns[2].y);
			painter.GraphicsState.SetCurrentMatrix(matrix);
			//cout <<"--set ctm: "<<matrix[0]<<", "<<matrix[1]<<", "<<matrix[2]<<", "<<matrix[3]<<", "<<matrix[4]<<", "<<matrix[5]<<endl;

			matrix = painter.GraphicsState.GetCurrentMatrix();
			//cout <<"--cur ctm: "<<matrix[0]<<", "<<matrix[1]<<", "<<matrix[2]<<", "<<matrix[3]<<", "<<matrix[4]<<", "<<matrix[5]<<endl;
			

			// auto type = child->get_type();
			// String type_name = Variant::get_type_name(type);
			// cout << "child "<<c<<" type: "<<type_name.utf8()<<endl;


			Polygon2D *polygon = Object::cast_to<Polygon2D>(child);
			if (polygon) {
				cout << "--FOUND Polygon2D"<<endl;


				PdfPainterPath path;
				PackedVector2Array points = polygon->get_polygon();
				for (int c2 = 0; c2<points.size(); c2++) {
					godot::Vector2 pt = points[c2];
					//------
					//godot::Vector2 pt = polygon->to_global(points[c2]);
					//if (paper_child) pt = paper_child->to_local(pt);

					//pt.y = page_height - pt.y;
					//pt *= 72.0;
					if (c2 == 0)
						path.MoveTo(pt.x, pt.y);
					else
						path.AddLineTo(pt.x, pt.y);
				}
				path.Close();
				Color color = polygon->get_color();
				fill_color = PdfColor(color.r, color.g, color.b);
				painter.GraphicsState.SetFillColor(fill_color);
				painter.DrawPath(path, PdfPathDrawMode::Fill);

				painter.Restore();
				continue;
			}

			Sprite2D *sprite = Object::cast_to<Sprite2D>(child);
			if (sprite) {
				cout << "--FOUND Sprite2D"<<endl;

				Ref<Texture2D> tex = sprite->get_texture();
				Ref<Image> tex_image = tex->get_image();
				PackedByteArray bytes = tex_image->get_data();
				//const char *godot_bytes = static_cast<const char *>(bytes._native_ptr());
				//const char *godot_bytes = reinterpret_cast<const char *>(bytes.ptr());
				const unsigned char *godot_bytes = bytes.ptr();

				int iw = tex_image->get_width();
				int ih = tex_image->get_height();
				int format = tex_image->get_format();

				// We need to strip out transparency from what godot::Image gives us,
				// as podofo doesn't implement transparent images directly.
				char rgb[iw*ih*3];
				int iold = 0;
				int inew = 0;
				for (int y=0; y<ih; y++) {
					for (int x=0; x<iw; x++) {
						iold = ((ih - y - 1)*iw + x) * 4; //flip y for some reason
						unsigned char a = godot_bytes[iold+3];
						rgb[inew  ] = a > 128 ? godot_bytes[iold]   : 255;
						rgb[inew+1] = a > 128 ? godot_bytes[iold+1] : 255;
						rgb[inew+2] = a > 128 ? godot_bytes[iold+2] : 255;
						// rgb[inew  ] = x < iw/3 ? 255 : (x < iw*2/3 ? 0: 0);
						// rgb[inew+1] = x < iw/3 ? 0 : (x < iw*2/3 ? 255: 0);;
						// rgb[inew+2] = x < iw/3 ? 0 : (x < iw*2/3 ? 0: 255);;
						inew += 3;
					}
				}

				auto image = document.CreateImage();
				cout << "  image format: "<<format<<endl;
				if (format == 5) { // support only RBGA8
					cout << "buffer..."<<endl;
					//bufferview buffer(godot_bytes, bytes.size());
					bufferview buffer(rgb, 3*iw*ih);
					cout << "image->SetData..."<<endl;
					//image->SetData(buffer, iw, ih, PdfPixelFormat::RGBA); *** RGBA UNIMPLEMENTED!
					image->SetData(buffer, iw, ih, PdfPixelFormat::RGB24);
					cout << "DrawImage..."<<endl;
					painter.DrawImage(*image, 0,0, 1,1);
				}
				cout << "  after DrawImage "<<endl;

				painter.Restore();
				continue;
			}

			Path2D *path = Object::cast_to<Path2D>(child);
			if (path) {
				cout << "--FOUND Path2D"<<endl;
				painter.Restore();
				continue;
			}

			Line2D *line = Object::cast_to<Line2D>(child);
			if (line) {
				cout << "--FOUND Line2D"<<endl;
				
				PdfPainterPath path;
				PackedVector2Array points = line->get_points();

				float width = line->get_width(); //in pixels in line space
				//float scale = paper_child->to_local(line->to_global(godot::Vector2(0,0))).distance_to(paper_child->to_local(line->to_global(godot::Vector2(width,0)))) / width;
				float scale = 1.0;


				for (int c2 = 0; c2<points.size(); c2++) {
					godot::Vector2 pt = points[c2];
	
					if (c2 == 0)
						path.MoveTo(pt.x, pt.y);
					else
						path.AddLineTo(pt.x, pt.y);
				}
				if (line->is_closed()) path.Close();
				Color color = line->get_default_color();
				stroke_color = PdfColor(color.r, color.g, color.b);
				painter.GraphicsState.SetStrokeColor(stroke_color);
				//painter.GraphicsState.SetFillColor(fill_color);
				painter.GraphicsState.SetLineWidth(scale * width);
				painter.DrawPath(path, PdfPathDrawMode::Stroke);

				painter.Restore();
				continue;
			}

			painter.Restore();
		}


        // Tell PoDoFo that the page has been drawn completely.
        // This is required to optimize drawing operations inside PoDoFo
        // and has to be done whenever you are done with drawing a page.
        painter.FinishDrawing();

        // Set some additional information on the PDF file.
        document.GetMetadata().SetCreator(PdfString("Cups printer for Godot"));
        document.GetMetadata().SetAuthor(PdfString("You"));
        //document.GetMetadata().SetTitle(PdfString("Hello World"));
        //document.GetMetadata().SetSubject(PdfString("Testing the PoDoFo PDF Library"));
        //document.GetMetadata().SetKeywords(vector<string>({ "Test", "PDF", "Hello World" }));


        if (is_to_file) {
	        // The last step is to close the document.
	        document.Save(static_cast<const char *>(destination.utf8()));
	        cout << "Current directory: "<<std::filesystem::current_path()<<", to file: "<<destination.utf8()<<endl;
	        UtilityFunctions::print("Print to file done!");

	    } else {
	    	// *** print via cups
	    	const char *temp_file = "TEMPORARY.pdf"; 
	    	document.Save(temp_file);

	    	// get the cups dest
			const char *dest_name = static_cast<const char *>(destination.utf8());
			//cups_dest_t *dests = nullptr;
			//int numdests = cupsGetDests2(CUPS_HTTP_DEFAULT, &dests);
			//cups_dest_t *dest = cupsGetDest(dest_name, nullptr, numdests, dests);
			
			cups_dest_t *dest = cupsGetNamedDest(CUPS_HTTP_DEFAULT, dest_name, nullptr);

			//cups_dinfo_t *dest_info = cupsCopyDestInfo(CUPS_HTTP_DEFAULT, &(dests[0]));
			cups_dinfo_t *dest_info = cupsCopyDestInfo(CUPS_HTTP_DEFAULT, dest);

			// now print
			bool dry_run = false;
			if (dest && dest_info)
				if (dry_run) {
	    			cout << "Pretending to print to "<< dest->name <<endl;
	    			UtilityFunctions::print("Pretending to print to "+String(dest->name));
				} else {
		    		cout << "Sending print to "<< dest->name <<endl;
		    		UtilityFunctions::print("Sending print to "+String(dest->name));
		    		PrintWithCups(temp_file, dest, dest_info, "Test doc", 1);
		    		cout << "Print command done"<<endl;
		    		UtilityFunctions::print("Print command done");
				}
	    	else throw ("Nuts! print failed! missing dest or dest_info!"); //FIXME: ***note mem leak potential

	    	cupsFreeDestInfo(dest_info);
	    	//cupsFreeDests(numdests, dests);
	    	cupsFreeDests(1, dest);
	    }
	}
	catch (PdfError& e)
    {
    	cout << "Podofo error, crapola! "<< e.GetName()<<endl;

        // All PoDoFo methods may throw exceptions
        // make sure that painter.FinishPage() is called
        // or who will get an assert in its destructor
        try
        {
            painter.FinishDrawing();
        }
        catch (...)
        {
            // Ignore errors this time
        }

        //throw e;
    }


	return 0;
}


#define DBG


void CheckJob(int job_id)
{
	cups_job_t *jobs = nullptr;
	int num_jobs = cupsGetJobs2(CUPS_HTTP_DEFAULT, &jobs, nullptr, 0, CUPS_WHICHJOBS_ACTIVE);
	if (num_jobs == 0) {
		cout << "no jobs"<<endl;
	} else {
		for (int c=0; c<num_jobs; c++) {
			cout << "job "<<jobs[c].id<<endl;
			cout << "  dest: "<<jobs[c].dest<<endl;
		}
	}
}


/*! Return 0 for success or nonzero error.
 */
int PrintWithCups(const char *filename, //!< Currently must be a pdf.
		  cups_dest_t *dest,
		  cups_dinfo_t *dest_info,
		  const char *title, //!< Such as "My Document"
		  int num_copies
		 )
{
	//first create a cups job
	char scratch[100];
	int job_id = 0;
	int num_options = 0;
	cups_option_t *options = NULL;
	sprintf(scratch, "%d", num_copies);

	num_options = cupsAddOption(CUPS_COPIES, scratch,
	                            num_options, &options); //if num_options==0, creates a new array
	//num_options = cupsAddOption(CUPS_MEDIA, CUPS_MEDIA_LETTER,
	//                            num_options, &options);
	//num_options = cupsAddOption(CUPS_SIDES,
	//                            CUPS_SIDES_TWO_SIDED_PORTRAIT,
	//                            num_options, &options);

	ipp_status_t status = cupsCreateDestJob(CUPS_HTTP_DEFAULT,
				  dest,
                  dest_info,
                  &job_id, //gets assigned during call
                  title,
                  num_options,
                  options //see cupsAddOption()
                  );

	CheckJob(job_id);

	if (status != IPP_STATUS_OK) {
		//no!!!!
		// *** make error message
		return 1;
	}

	//------now send the file data to the job
	const char *format = nullptr;
	format = CUPS_FORMAT_PDF;
	// format = CUPS_FORMAT_POSTSCRIPT;
	// format = CUPS_FORMAT_JPEG;
	// format = CUPS_FORMAT_TEXT;

	FILE *fp = fopen(filename, "rb");
	if (!fp) {
		// *** Error!!
		return 2;
	}

	size_t bytes;
	char buffer[65536];
	int errored = 0;

	try {
		http_status_t pstatus = cupsStartDestDocument(CUPS_HTTP_DEFAULT,
							  dest,
		                      dest_info,
		                      job_id,
		                      filename, //name of doc, "typically the original filename"
		                      format, //mime type of original document.. ok for null
		                      num_options,
		                      options,
		                      1); // last_document: 1 if this request is the last document for job

		if (pstatus == HTTP_STATUS_CONTINUE) {
			while ((bytes = fread(buffer, 1, sizeof(buffer), fp)) > 0) {
			    if (cupsWriteRequestData(CUPS_HTTP_DEFAULT, buffer, bytes) != HTTP_STATUS_CONTINUE)
			    	break;
			}

			if (cupsFinishDestDocument(CUPS_HTTP_DEFAULT, dest, dest_info) == IPP_STATUS_OK) {
				DBG cerr << "Print job sent to printer"<<endl;
			} else {
				throw cupsLastErrorString();
			}

		} else {
			// could not start dest! fail!
			//throw _("Could not start print job!");
			throw "Could not start print job!";
		}

	} catch (const char *msg) {
		DBG cerr << "Print error: "<<(msg?msg:"")<<endl;
		errored = 2;

	} catch (exception& e) {
		// *** some other error
		DBG cerr << "Some kind of print error: "<<e.what() <<endl;
		errored = 1;
	}

	fclose(fp);
	return errored;
}

