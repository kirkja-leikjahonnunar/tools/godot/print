#pragma once

#include <godot_cpp/classes/object.hpp>
#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/classes/node2d.hpp>

using namespace godot;

class PrintingSingleton : public Object
{
	GDCLASS(PrintingSingleton, Object);

	static PrintingSingleton *singleton;

protected:
	static void _bind_methods();

public:
	static PrintingSingleton *get_singleton();

	PrintingSingleton();
	~PrintingSingleton();

	void hello_singleton();
	godot::TypedArray<String> GetCupsDestinations();
	int Print(Node2D *canvas, String destination, bool is_to_file, float offset_y);
};
