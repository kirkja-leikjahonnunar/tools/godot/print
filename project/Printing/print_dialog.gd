extends Control


@export var default_to_file := "out.pdf"


signal print_now(printer_name: String, is_to_file: bool)


var printer_list := []


# Called when the node enters the scene tree for the first time.
func _ready():
	visibility_changed.connect(_on_visibility_changed)
	$"VBoxContainer/Final Buttons/CancelButton".pressed.connect(CancelDialog)
	$"VBoxContainer/Final Buttons/PrintButton".pressed.connect(Print)
	
	if has_node("../UI/Print"):
		get_node("../UI/Print").pressed.connect(StartDialog)


func Print():
	print ("Printing...")
	var printer_list_node : ItemList = $VBoxContainer/PrinterList
	var selected = printer_list_node.get_selected_items()
	print ("selected: ", selected)
	if selected.size() == 0:
		print ("nothing selected, don't print!!")
	else:
		if selected[0] >= printer_list.size():
			var dir = DirAccess.open(".")
			print ("print to file: ", dir.get_current_dir(), "/", default_to_file)
			print_now.emit(default_to_file, true)
		else:
			print ("print to printer: ", printer_list[selected[0]])
			print_now.emit(printer_list[selected[0]], false)
	
	# clean up and turn off dialog
	CancelDialog()
	print ("Print function gdscript done!")


func CancelDialog():
	visible = false
	if has_node("../UI"):
		get_node("../UI").visible = true

func StartDialog():
	if has_node("../UI"):
		get_node("../UI").visible = false
	visible = true


func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed == true && event.keycode == KEY_P:
			print ("Print!!")
			StartDialog()
		if event.pressed == true && event.keycode == KEY_ESCAPE:
			CancelDialog()


func _on_visibility_changed():
	print("print dialog visible change: ", visible)
	if not visible:
		return
	
	printer_list = PrintingSingleton.GetCupsDestinations()
	
	var printer_list_node : ItemList = $VBoxContainer/PrinterList
	printer_list_node.clear()
	
	print ("number of printers found: ", printer_list.size())
	
	for item in printer_list:
		print("printer: ", item)
		printer_list_node.add_item(item)
	
	printer_list_node.add_item("To PDF file")
	
	printer_list_node.select(printer_list_node.item_count-1)
