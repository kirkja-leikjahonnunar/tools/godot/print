extends Polygon2D


@export var width  := 8.5
@export var height := 11.0

func SetPaperSize(width: float, height: float) -> void:
	if width <= 0 || height <= 0:
		push_error("Bad width or height: ", width, " x ", height)
		return
	
	self.width = width
	self.height = height
	
	polygon[0] = Vector2.ZERO
	polygon[1] = Vector2(width, 0)
	polygon[2] = Vector2(width, height)
	polygon[3] = Vector2(0, height)


#func _input(event):
	#if event is InputEventMouseMotion:
		# #var paper_position : Vector2 = to_local(event.position)
		#var paper_position : Vector2 = transform.affine_inverse() * event.position
		#print ("mouse on paper: ", paper_position)
