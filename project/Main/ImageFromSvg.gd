@tool
extends Sprite2D


@export_multiline var svg: String = """<svg width="100px" height="100px" viewBox="0 0 100 100">
<path d="M 10 10 C 20 20, 40 20, 50 10" stroke="black" fill="transparent"/>
</svg>"""


@export var do_it := false:
	set(value):
		if value:
			RenderSvg()


func RenderSvg():
	var image = texture.get_image()
	print (image)
	var status = image.load_svg_from_string(svg)
	print ("load_svg_from_string return status: ", status)
	
	var tex = ImageTexture.create_from_image(image)
	texture = tex

