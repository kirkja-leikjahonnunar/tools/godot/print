@tool
extends Line2D

@export var update_now := false:
	set(value):
		if value:
			UpdateLine()

func _ready():
	UpdateLine()


func UpdateLine():
	var path : Path2D = get_parent()
	var curve := path.curve
	var pts := curve.get_baked_points()
	points = pts
	
