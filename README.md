# Example to print something out from a running Godot game

This extension prototypes rendering 2D Godot objects to a PDF
(currently using libpodofo), then sending that to a printer through CUPS.

Godot objects renderable so far:
 - Polygon2D
 - Line2D
 - Sprite2D (non-transparent)

Planned objects:
 - Sprite2D (with transparency)
 - Bezier curves, based loosely on Path2D


### Getting started:

1. Clone this repository with submodules.
    - `git clone --recurse-submodules https://gitlab.com/kirkja-leikjahonnunar/tools/godot/print.git`
    - `cd print`
2. Update to the latest `godot-cpp`.
    - `git submodule update --remote`
	If you forgot to recurse submodules when you cloned, you can init them after cloning:
	- git submodule update --init --recursive
3. Make sure you have the libpodofo and libcups development packages installed 
   on your machine. For instance, to ensure this on Debian based linux distros, run this:
    - `sudo apt install libcups2-dev libpodofo-dev`.
3. Build a debug binary for the current platform. Note this extension uses exceptions, so:
	- `scons disable_exceptions=false`
4. Import, edit, and play `project/` using Godot Engine 4+.
    - `godot --path project/`

### Repository structure:
- `project/` - the Godot project.
  - `addons/example/` - Files to be distributed to other projects.
  - `demo/` - Scenes and scripts for internal testing. Not strictly necessary.
- `src/` - Source code of this extension.
- `godot-cpp/` - Submodule needed for GDExtension compilation.


